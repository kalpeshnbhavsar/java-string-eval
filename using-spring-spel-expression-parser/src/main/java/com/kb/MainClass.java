package com.kb;

import com.commons.*;

public class MainClass {

    public static void main(String[] args) {

        MyLogger log = MyLogger.getInstance();

        String strExp = "1+2*23";

        Object o = MySpelExpressionParser.parseExpression(strExp, Double.class.getName());
        log.info(o);
    }
}