package kb;

import com.commons.*;
import com.commons.MyScriptEngine;

import javax.script.ScriptEngine;
import javax.script.ScriptException;

public class MainClass {

    public static void main(String[] args) {

        MyLogger log = MyLogger.getInstance();
        ScriptEngine engine = MyScriptEngine.getInstance();

        try {
            log.info(engine.eval("1 + 2"));
        } catch (ScriptException e) {
            log.info(e.getMessage());
        }
    }
}