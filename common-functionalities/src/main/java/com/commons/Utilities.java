package com.commons;

import java.util.regex.Pattern;

class Utilities {

    private Utilities(){}

    static boolean isNumericExpression(String strExp){
        return Pattern.matches("[0-9+\\+\\-\\*/\\(\\) ]*",strExp);
//        return Pattern.matches("(\\d+)(\\.)?(\\d+)?",strExp);
    }
}
