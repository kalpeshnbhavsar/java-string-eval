package com.commons;

import org.springframework.expression.spel.standard.SpelExpressionParser;

public abstract class MySpelExpressionParser {

    private static SpelExpressionParser parser = new SpelExpressionParser();

    private MySpelExpressionParser() {
    }

    public static SpelExpressionParser getInstance() {
        return parser;
    }

    public static Object parseExpression(String expression, String valueType) {
        if (Utilities.isNumericExpression(expression))
            return parser.parseExpression(expression).getValue(valueType.getClass());
        else return null;
    }
}
