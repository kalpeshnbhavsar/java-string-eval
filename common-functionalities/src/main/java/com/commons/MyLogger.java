package com.commons;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyLogger {
    private static MyLogger ourInstance = new MyLogger();
    private Logger log;

    public static MyLogger getInstance() {
        return ourInstance;
    }

    private MyLogger() {
        log = LoggerFactory.getLogger(MyLogger.class);
    }

    public void info(Object o){
        log.info(String.valueOf(o));
    }
}
