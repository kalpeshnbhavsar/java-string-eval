package com.commons;

import javax.script.ScriptEngineManager;
import javax.script.ScriptEngine;

public abstract class MyScriptEngine {

    private static ScriptEngine ourInstance = new ScriptEngineManager().getEngineByName("JavaScript");

    public static ScriptEngine getInstance() {
        return ourInstance;
    }

    private MyScriptEngine() {
    }
}
