# Java string evaluator

How to evaluate string as mathematical expression

## Installation

You need to have maven installed on your machine to run this project.
Please built the project before running

```bash
mvn clean install
```

## Usage

``` java
Sanity Check!
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## License
No License (Keep up the good work:))
